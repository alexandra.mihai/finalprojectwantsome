@UI @CartTests
Feature: Add/Remove/Sort Products Test Scenarios

  Scenario: User adds one product to cart

    Given The user logs in with username "standard_user" and password "secret_sauce"
    When The user adds a product to cart
    Then The product is added to the cart successfully

  Scenario: User adds two products to cart

    Given The user logs in with username "standard_user" and password "secret_sauce"
    When The user adds a product to cart
    And The user adds a second product to cart
    Then The product is added to the cart successfully

  Scenario: User adds three products to cart

    Given The user logs in with username "standard_user" and password "secret_sauce"
    When The user adds a product to cart
    And The user adds a second product to cart
    And The user adds a third product to cart
    Then The product is added to the cart successfully

  Scenario: User removes a product from cart

    Given The user logs in with username "standard_user" and password "secret_sauce"
    When The user adds a product to cart
    And The user removes a product from cart
    Then The product is removed from the cart successfully

  Scenario: User sorts products descending

    Given The user logs in with username "standard_user" and password "secret_sauce"
    When The user selects the descending sorting option
    Then The first product displayed is a red t-shirt