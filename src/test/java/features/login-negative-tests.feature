@UI @Negative
Feature: Login Negative Test Scenarios

  Scenario: User leaves both username and password fields empty

    Given The user is on the login page
    When The user leaves the username field empty
    And The user leaves the password field empty
    And The user clicks the login button
    Then An error message is displayed

  Scenario: User enters a username with no password

    Given The user is on the login page
    When The user enters a valid username "standard_user"
    And The user leaves the password field empty
    And The user clicks the login button
    Then An error message is displayed

  Scenario: User logs in with incorrect username and incorrect password

    Given The user is on the login page
    When The user enters an invalid username "invalid_user"
    And The user enters an invalid password "wrongPassword"
    And The user clicks the login button
    Then An error message is displayed

  Scenario: User logs in with correct username and incorrect password

    Given The user is on the login page
    When The user enters a valid username "standard_user"
    And The user enters an invalid password "wrong_password"
    And The user clicks the login button
    Then An error message is displayed

  Scenario: User logs in with incorrect username and correct password

    Given The user is on the login page
    When The user enters an invalid username "invalid_user"
    And The user enters a valid password "secret_sauce"
    And The user clicks the login button
    Then An error message is displayed



