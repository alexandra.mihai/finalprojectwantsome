@UI @Positive
Feature: Login Positive Test Scenarios

  Scenario: User logs in with valid credentials

    Given The user is on the login page
    When The user enters a valid username "standard_user"
    And The user enters a valid password "secret_sauce"
    And The user clicks the login button
    Then The user is redirected to the dashboard


  Scenario Outline: Login on the page with correct credentials with multiple examples

    Given The user is on the login page
    When The user enters a valid username "<username>"
    And The user enters a valid password "<password>"
    And The user clicks the login button
    Then The user is redirected to the dashboard

    Examples:
      | username                | password     |
      | performance_glitch_user | secret_sauce |
      | visual_user             | secret_sauce |