package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pomz.LoginPage;

import static stepdefinitions.Hooks.driver;

public class LoginSteps {

    LoginPage loginPage;

    @Given("The user is on the login page")
    public void beOnLoginPage() {
        loginPage = new LoginPage(driver);
        driver.get(loginPage.url);
    }

    @When("The user enters a valid username {string}")
    public void theUserEntersAValidUsername(String username) {
        loginPage.fillUserName(username);
    }

    @And("The user enters a valid password {string}")
    public void theUserEntersAValidPassword(String password) {
        loginPage.fillPassword(password);
    }

    @And("The user clicks the login button")
    public void theUserClicksTheLoginButton() {
        loginPage.clickOnLoginButton();
    }

    @Then("The user is redirected to the dashboard")
    public void theUserIsRedirectedToTheDashboard() {
        Assert.assertEquals("https://www.saucedemo.com/inventory.html", driver.getCurrentUrl());
    }

    @When("The user leaves the username field empty")
    public void theUserLeavesTheUsernameFieldEmpty() {
        loginPage.fillUserName("");
    }

    @And("The user leaves the password field empty")
    public void theUserLeavesThePasswordFieldEmpty() {
        loginPage.fillPassword("");
    }

    @Then("An error message is displayed")
    public void anErrorMessageIsDisplayed() {
        Assert.assertTrue("Error message is not displayed", loginPage.getErrorMessage());
    }

    @When("The user enters an invalid username {string}")
    public void theUserEntersAnInvalidUsername(String invalidUsername) {
        loginPage.fillUserName(invalidUsername);
    }

    @And("The user enters an invalid password {string}")
    public void theUserEntersAnInvalidPassword(String invalidPassword) {
        loginPage.fillPassword(invalidPassword);
    }
}
