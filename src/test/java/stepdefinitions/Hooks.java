package stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class Hooks {

    protected static WebDriver driver;
    protected static WebDriverWait wait;

    @Before
    public void setupBefore() {
        System.out.println("Initialize driver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
       // wait = new WebDriverWait(driver, Duration.ofSeconds(150));
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));

    }

    @After
    public void tearDownAfter() {
        System.out.println("Quit the page");
        driver.quit();
    }
}
