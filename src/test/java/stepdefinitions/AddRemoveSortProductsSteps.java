package stepdefinitions;


import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import pomz.LoginPage;
import pomz.ProductsPage;

import static stepdefinitions.Hooks.driver;

public class AddRemoveSortProductsSteps {

    LoginPage loginPage = new LoginPage(driver);
    ProductsPage productsPage = new ProductsPage(driver);

    @Given("The user logs in with username {string} and password {string}")
    public void loginWithCredentials(String username, String password) {
        //      loginPage = new LoginPage(driver);
        driver.get(LoginPage.url);
        loginPage = new LoginPage(driver);
        loginPage.fillUserName(username);
        loginPage.fillPassword(password);
        loginPage.clickOnLoginButton();
    }

    @When("The user adds a product to cart")
    public void theUserAddsAProductToCart() {
        //       productsPage = new ProductsPage(driver);
        productsPage.clickOnAddToCartButton();
    }

    @Then("The product is added to the cart successfully")
    public void theProductIsAddedToTheCartSuccessfully() {
        productsPage.badgeIsDisplayedOnCart();
    }

    @And("The user adds a second product to cart")
    public void theUserAddsASecondProductToCart() {
        productsPage.clickOnSecondAddToCartButton();
    }

    @And("The user adds a third product to cart")
    public void theUserAddsAThirdProductToCart() {
        productsPage.clickOnThirdAddToCartButton();
    }

    @And("The user removes a product from cart")
    public void theUserRemovesAProductFromCart() {
        productsPage.clickOnRemoveFirstAddToCartButton();
    }

    @Then("The product is removed from the cart successfully")
    public void theProductIsRemovedFromTheCartSuccessfully() {
        productsPage.badgeIsNotDisplayedOnCart();
    }

    @When("The user selects the descending sorting option")
    public void theUserSelectsTheDescendingSortingOption() {
        productsPage.clickOnSortButton();
        WebElement dropdown = driver.findElement(productsPage.sortButton);
        Select dropdownSelect = new Select(dropdown);
        dropdownSelect.selectByIndex(2);
    }

    @Then("The first product displayed is a red t-shirt")
    public void theFirstProductDisplayedIsTestAllTheThingsTShirtRed() {
        productsPage.descendingItemIsDisplayed();
    }
}
