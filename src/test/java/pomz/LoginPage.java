package pomz;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {

    private WebDriver driver;
    public static String url = "https://www.saucedemo.com/";

    public By userName = By.xpath("//*[@id=\"user-name\"]");
    public By password = By.xpath("//*[@id=\"password\"]");
    public By loginButton = By.xpath("//*[@id=\"login-button\"]");
    public By errorIncorrectFields = By.xpath("//*[@id=\"login_button_container\"]/div/form/div[3]/h3");


    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public void fillUserName(String username) {
        driver.findElement(this.userName).sendKeys(username);
    }

    public void fillPassword(String password) {
        driver.findElement(this.password).sendKeys(password);
    }

    public void clickOnLoginButton() {
        driver.findElement(this.loginButton).click();
    }

    public boolean getErrorMessage() {
        return driver.findElement(errorIncorrectFields).isDisplayed();
    }

}
