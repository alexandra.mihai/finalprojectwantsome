package pomz;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.WebElement;


public class ProductsPage {

    private WebDriver driver;
    public static String url = "https://www.saucedemo.com/";


    public By addToCartButton = By.xpath("//*[@id=\"add-to-cart-sauce-labs-backpack\"]");
    public By secondAddToCartButton = By.xpath("//*[@id=\"add-to-cart-sauce-labs-bolt-t-shirt\"]");
    public By thirdAddToCartButton = By.xpath("//*[@id=\"add-to-cart-sauce-labs-fleece-jacket\"]");
    public By removeFirstAddToCartButton = By.xpath("//*[@id=\"remove-sauce-labs-backpack\"]");
    public By cartWithBadge = By.xpath("//*[@id=\"shopping_cart_container\"]/a/span");
    public By cartWithNoBadge = By.xpath("//*[@id=\"shopping_cart_container\"]");

    public By sortButton = By.xpath("//*[@id=\"header_container\"]/div[2]/div/span/select");

    public By descendingItemDisplayed = By.xpath("//*[@id=\"item_3_title_link\"]/div");

    public ProductsPage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickOnAddToCartButton() {
        driver.findElement(this.addToCartButton).click();
    }

    public void badgeIsDisplayedOnCart() {
        driver.findElement(this.cartWithBadge).isDisplayed();
    }

    public void clickOnSecondAddToCartButton() {
        driver.findElement(this.secondAddToCartButton).click();
    }

    public void clickOnThirdAddToCartButton() {
        driver.findElement(this.thirdAddToCartButton).click();
    }

    public void clickOnRemoveFirstAddToCartButton() {
        driver.findElement(this.removeFirstAddToCartButton).click();
    }

    public void badgeIsNotDisplayedOnCart() {
        driver.findElement(this.cartWithNoBadge).isDisplayed();
    }

    public void clickOnSortButton() {
        driver.findElement(this.sortButton).click();
    }

    public void descendingItemIsDisplayed() {
        driver.findElement(this.descendingItemDisplayed).isDisplayed();
    }
}
